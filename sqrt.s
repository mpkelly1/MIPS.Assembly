# # Michael Kelly
# # UNO CSCI 3301-  Fall 2017
# # Last Updated:  October 12, 2017
# # Square-root non-leaf


# # Data Declarations

.data

prompt:	.ascii	"Square-Root Example Program\n\n"
		.asciiz "Enter value to compute square-root: "
results: .asciiz "\n\nSquare-root is approximately:  "
negativeString:	.asciiz	" * square-root of 'i'"

x: 			.float 0.0
precision:	.float 0.2
answer: 	.float 0.0


# # Text/code section
.text
.globl	main
main:

# # Read x value from user

li $v0, 4 # print prompt string
la $a0, prompt
syscall
# # Try reading in as a float, see if it works for integers
li $v0, 6 # read X (as float)
syscall
# # Move that FP value in $f0 to $f16 for later use
	## First move and convert zero into FP register
mtc1	$zero, $f18
## convert to FP
cvt.s.w	$f18, $f18
add.s	$f16, $f0, $f18
## Convert to absolute value
abs.s	$f0, $f0
## Save that value into "x" variable
s.s $f0, x  # $f0 contains user input, save to var: x

#### Step 1:  Divide the user's input by 2.
####
## put an integer "2" in register t0 (for divide)
addi $t0, $zero, 2
## move that integer into a FPregister $f10
mtc1	$t0, $f10
## MUST Convert Integer in FPreg to FP.s-precision
cvt.s.w	$f12, $f10
# # Now divide $f0(input) by $f12(2)
div.s	$f12, $f0, $f12


# #  Call PROC: "rootStart"
jal rootStart

# # Display result
li $v0, 4 # print prompt string
la $a0, results
syscall
li $v0, 2 # print float
l.s $f12, answer
syscall

## Now, print "negativeString" if $f16 < 0
c.lt.s	$f16, $f18
bc1t	negPrint
# # Done, terminate program.

li $v0, 10 # call code for terminate
syscall # system call

.end	main


#############
############


.globl	root

.ent	root


negPrint:
li $v0, 4 # print prompt string
la $a0, negativeString
syscall
## DONE:  Terminate program
li $v0, 10 # call code for terminate
syscall # system call


rootLower:
## put an integer "2" in register t0
addi $t0, $zero, 2 
## move that integer into a FPregister $f10
mtc1	$t0, $f10
## convert to FP
cvt.s.w	$f10, $f10
## Now divide $f12 by $f10, store in $f0
div.s	$f12, $f12, $f10
# # Now add Quotient + Remainder = $f12
j	root




rootUpper:
## Multiply quotient by two (adder op)
add.s	$f14, $f12, $f12
## Then add that result to itself
add.s	$f12, $f14, $f12
## Now divide, $f12 by $f10 (2)
div.s	$f12, $f12, $f10
## Call root
j	root ## New mid-point saved in $f12




rootStart:
subu $sp, $sp, 4
sw $ra, ($sp) ##first stack operation





root:
# # Print the mid-point (double)
li	$v0, 2
syscall  ## Should print mid-point in $f12

# # Check if ($f12 x $f12 = $f3) < "x"
mul.s	$f3, $f12, $f12
# # Subtract the square of the half from "x"s
	# # 1st.  Load val of "x" into an FPreg.
l.s	$f2, x
	# # 2nd:  Subtract x - $f3
	# # 3rd:  Compare, branch when necessary
sub.s	$f0, $f2, $f3
# # Check absolute value of result: $f0 = abs value.
abs.s	$f0, $f0
# # Compare $f2 < "precision" ??  rootDone if so...
	# # 1st:  Move "precision" into FPreg-> $F4
l.s	$f4, precision
c.lt.s $f0, $f4
# # If the value of $f0 is less than "precision", j-> rootDone
bc1t	rootDone  # # Correct Answer = $f12

# # If not within range, is $f3 > $f2?
	# # True?  Then range should be $f12 / 2 
	# # False?  Then range should be $f12 + $f0 / 2
c.lt.s $f2, $f3
bc1t rootLower  # # If calculated > "x", then divide calc/2
bc1f rootUpper	# # If calc. < "x" then calc + x


rootDone: 
##  Now, answer in $f12, save into "answer"
s.s	$f12, answer	## ROOT DONE JUST ENTERED!
##lw $ra, ($sp)
#l.s $s0, 4($sp)
##addu $sp, $sp, 4
lw $ra, ($sp)
addu $sp, $sp, 4
jr $ra
.end	root