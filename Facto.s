# # Michael Kelly
# # UNO CSCI 3301-  Fall 2017
# # Last Updated:  October 8, 2017
# # Factorials using Recursion

# Example program to demonstrate recursion.

# # Data Declarations

.data

prompt:	.ascii	"Factorial Example Program\n\n"
		.asciiz "Enter N value: "
results: .asciiz "\nFactorial of N = "
# # MKelly...
negvalue: .asciiz "\nError:  Cannot compute negative factorial!  Try again!\n\n"
n: 			.word 0
answer: 	.word 0


# # Text/code section
.text
.globl	main
main:

# # Read n value from user

li $v0, 4 # print prompt string
la $a0, prompt
syscall
li $v0, 5 # read N (as integer)
syscall
sw $v0, n
# # Mkelly:  Now check if arg[0] [$v0] < 0
# # Mkelly:  If arg<0 , print error, loop main
lw	$a0, n
slt	$t0, $v0, $zero  ##branch instruction
bne	$t0, $zero, badvalue

# # Call factorial function.
lw $a0, n  ##Start : Call Factorial
jal fact
sw $v0, answer

# # Display result

li $v0, 4 # print prompt string
la $a0, results
syscall
li $v0, 1 # print integer
lw $a0, answer
syscall
# # Done, terminate program.

li $v0, 10 # call code for terminate
syscall # system call

.end	main



#  MKelly:  Directive Label for invalid
#			condition value.
.globl	badvalue

.ent	badvalue
badvalue:
li	$v0, 4
la	$a0, negvalue
syscall
jal	main

.end	badvalue



# # Factorial function
# Recursive definition:
# = 1 if n = 0
# = n * fact(n1) if n >= 1
# # Arguments
# $a0 n
# Returns
# $v0 set to n!
.globl	fact

.ent	fact
fact:
subu $sp, $sp, 8
sw $ra, ($sp)
sw $s0, 4($sp)
li $v0, 1 # check base case
beq $a0, 0, factDone
move $s0, $a0 # fact(n1)
sub $a0, $a0, 1
jal fact
mul $v0, $s0, $v0 # n * fact(n1)

factDone:
lw $ra, ($sp)
lw $s0, 4($sp)
addu $sp, $sp, 8
jr $ra
.end	fact